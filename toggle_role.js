/** Check to see if a cookie is set.
* If not set, default to showing all role functions. Set cookie to show roles.
* If set, show or hide role functions based on cookie value.
* If toggle is clicked, set the cookie to the opposite value of current.
**/
jQuery(document).ready(function(){
	
	// Get Opposite string.
	str_opposite = get_visibility_opposite();
	
	jQuery("#toggle_role_block_link").bind("click",function(){
		toggle_role();
		return false; // don't follow the link
	});
	
	// Set the current visibility
	set_role();

});

/**
 * Shows the admin elements, sets the cookie, and changes the toggle button to Hide
 */
function show_elements(){
	
	// 1. Reload the saved inline padding-top (if exists).
	if (jQuery.cookie("permission_toggle_padding")) {
		jQuery("body").css("padding-top",jQuery.cookie("permission_toggle_padding"));
		jQuery.cookie("permission_toggle_padding",null, {path: "/"}); // delete the cookie as it isn't needed (and if the value ever changes, this reset will get the plugin back on track)
	}
	
	// 2. Reload the saved class values from CSS.
	var array_class = jQuery.cookie("permission_toggle_classes");
	if (array_class){
		array_class = array_class.split(","); //convert from comma string to array
		var length = array_class.length;
		for (var i=0; i<length; i++){
			jQuery("body").addClass(array_class[i]);
		}
	}
	
	// 3. Show the remaining elements.
	jQuery("div.region.region-page-top").show(); //some skins put the toolbar inside this div
	jQuery("#toolbar").show();
	jQuery("html.js div.contextual-links-wrapper").show();
	jQuery("div.tabs").has("ul.tabs.primary li a:contains('View')").show();
	jQuery("div.clearfix.tabs-wrapper, .block-workbench").show();
	
	
	
	
	// 4. Change the Toggle link text
	jQuery("#toggle_role_block_link").text("Hide Admin");
	
	// 5. Set the cookie to the current value.
	jQuery.cookie("permission_toggle_value","Show", {expires: 90, path: "/"});
}

/**
 * Hides the admin elements, sets the cookie, and changes the toggle button to Show
 */
function hide_elements(){

	// 1. Save the current inline CSS padding-top and remove it
	//if (jQuery("body").prop("style")["paddingTop"]){ // this requires jQuery 1.5 or higher
	if (get_style_attr(jQuery("body")) && get_style_attr(jQuery("body"))["padding-top"]){
		// inline style is set. save it and remove it
		jQuery.cookie("permission_toggle_padding",jQuery("body").css("padding-top"),{expires: 90, path: "/"});
		jQuery("body").css("padding-top",""); // delete the inline style
	} else {
		// no inline style. clear the cookie to make sure there aren't old values.
		jQuery.cookie("permission_toggle_padding",null, {path: "/"});
	}
	
	// 2. Remove the toolbar classes so that the css padding is correct.
	var class_array = new Array();
	if (jQuery("body").hasClass('toolbar')) {
		jQuery("body").removeClass('toolbar');
		 class_array.push("toolbar");
	}
	if (jQuery("body").hasClass('toolbar-drawer')) {
		jQuery("body").removeClass('toolbar-drawer');
		 class_array.push("toolbar-drawer");
	}
	
	
	// 3. Save the classes that we removed into a cookie.
	class_array = jQuery.unique(class_array).join(",");  // delete duplicate values and convert array to comma string
	jQuery.cookie("permission_toggle_classes",class_array, {expires: 90, path: "/"});
	
	jQuery("div.region.region-page-top").hide(); //some skins put the toolbar inside this div
	jQuery("#toolbar").hide();
	jQuery("html.js div.contextual-links-wrapper").hide();
	jQuery("div.tabs").has("ul.tabs.primary li a:contains('View')").hide();
	jQuery("div.clearfix.tabs-wrapper, .block-workbench").hide();
	
	// 4. Change the Toggle link text
	jQuery("#toggle_role_block_link").text("Show Admin");
	
	// 5. Set the cookie to the current value.
	jQuery.cookie("permission_toggle_value","Hide", {expires: 90, path: "/"});
	
}
/**
 * Switches the current role visibility. If showing, then hides, and vice versa
 */
function toggle_role(){
	// swap to hide
	current = jQuery.cookie("permission_toggle_value");
	if (current == "Show"){
		hide_elements();
	} else if (current == "Hide") {
		show_elements();
	} else {
		// cookie not set. if not set, then it is currently showing. therefore, switch to hiding
		hide_elements();
	}
}
/**
 * Sets the current role visibility. Should only need to be run on page load.
 */
function set_role(){
	current = jQuery.cookie("permission_toggle_value");
	if (current == "Show"){
		show_elements();
	} else if (current == "Hide") {
		hide_elements();
	} else {
		// cookie not set. if not set, then default to show
		show_elements();
	}
}

/**
 * Returns the opposite string if the current visibility.
 * @returns {String}
 */
function get_visibility_opposite(){
	current = jQuery.cookie("permission_toggle_value");
	if (current == "Show"){
		return "Hide";
	} else if (current == "Hide") {
		return "Show";
	} else {
		// cookie not set. if not set, then everything is showing, so return Hide
		return "Hide";
	}
}

/**
 * Gets any inline style attribute on an object. This is needed for jQuery 1.4,
 * which is what Drupal 7 comes with. If jQuery 1.5 or greater is installed, 
 * the "prop" function is much easier to use.
 * @param attribute_name The jQuery object returned from most jQuery functions. 
 */
function get_style_attr(jQuery_object){
	style_string = jQuery_object.attr("style");
	
	if (style_string){
		style_array = style_string.split(";"); // convert string into a 1-dimensional array
		length = style_array.length;
		var style_object = new Array(); // array with key-value
		for (var i = 0; i < length; i++){
		    key_value = style_array[i].split(":"); // convert each array item into a key-value
		    if (key_value.length = 2){
		        style_object[jQuery.trim(key_value[0])] = jQuery.trim(key_value[1]);
		    }
		}
		return style_object; // all style attributes are now in key-value array format
	}
	return null; // style attribute was empty or null, so return null
	
}
